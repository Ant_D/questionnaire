from django.db import models


CONTENT_MAX_LENGTH = 1 << 20


class Question(models.Model):
    content = models.TextField(max_length=CONTENT_MAX_LENGTH)

    def __str__(self):
        return self.content


class Answer(models.Model):
    content = models.TextField(max_length=CONTENT_MAX_LENGTH)

    def __str__(self):
        return self.content


class Solution(models.Model):
    content = models.TextField(max_length=CONTENT_MAX_LENGTH)

    def __str__(self):
        return self.content


class Transition(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    next_question = models.ForeignKey(Question,
                                      blank=True, null=True,
                                      on_delete=models.SET_NULL,
                                      related_name="incoming_transition_set")
    solution = models.ForeignKey(Solution, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = ("question", "answer")


class Questionnaire(models.Model):
    name = models.CharField(max_length=256)
    first_question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
