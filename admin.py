from django.contrib import admin

# Register your models here.

from .models import Answer, Question, Questionnaire, Solution, Transition


class TransitionInline(admin.StackedInline):
    model = Transition
    extra = 0
    fk_name = "question"


class QuestionAdmin(admin.ModelAdmin):
    inlines = [TransitionInline]
    search_fields = ["content"]


admin.site.register(Answer)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Questionnaire)
admin.site.register(Solution)