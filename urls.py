from django.urls import path

from . import views


app_name = "questionnaire"
urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('<int:pk>/', views.DetailView.as_view(), name="detail"),
    path('question/<int:pk>/', views.QuestionView.as_view(), name="question_detail"),
    path('solution/<int:pk>/', views.SolutionView.as_view(), name="solution_detail"),
    path('transition/<int:pk>/solution_and_next_question', views.SolutionAndNextQuestionView.as_view(), name="solution_and_next_question"),
]
