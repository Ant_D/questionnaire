from django.db.utils import IntegrityError
from django.test import TestCase
from django.urls import reverse

from .models import Answer, Question, Questionnaire, Solution, Transition


def create_object(cls, content):
    """ Create an object with the given content. """
    return cls.objects.create(content=content)


def create_questionnaire(name, first_question):
    """ Create a questionnaire with the given name and first question. """
    return Questionnaire.objects.create(name=name, first_question=first_question)


def create_transition(question, answer, next_question=None, solution=None):
    return Transition.objects.create(question=question, answer=answer,
                                     next_question=next_question, solution=solution)


def create_minimal_transition():
    """ Create transition with question and answer
    and without next question and solution. """
    question = create_object(Question, "question content")
    answer = create_object(Answer, "answer content")
    return create_transition(question, answer, None, None)


def create_maximal_transition():
    question = create_object(Question, "question content")
    answer = create_object(Answer, "answer content")
    next_question = create_object(Question, "next question content")
    solution = create_object(Solution, "solution content")
    return create_transition(question=question, answer=answer,
                             next_question=next_question, solution=solution)


class QuestionnaireModelTests(TestCase):
    def test_questionnaire_is_removed_when_first_question_is_removed(self):
        question = create_object(Question, "test question")
        create_questionnaire("test questionnaire", question)
        question.delete()
        self.assertFalse(Questionnaire.objects.exists())

    def test_inability_to_create_questionnaire_without_first_question(self):
        try:
            create_questionnaire("test questionnaire", None)
        except IntegrityError:
            pass
        else:
            self.fail()


class TransitionModelTests(TestCase):
    def test_ability_to_create_transition_without_next_question_and_solution(self):
        try:
            create_minimal_transition()
        except IntegrityError:
            self.fail()

    def test_transition_is_removed_when_question_is_removed(self):
        transition = create_minimal_transition()
        transition.question.delete()
        self.assertFalse(Transition.objects.exists())

    def test_transition_is_removed_when_answer_is_removed(self):
        transition = create_minimal_transition()
        transition.answer.delete()
        self.assertFalse(Transition.objects.exists())

    def test_transition_is_not_removed_when_next_question_is_removed(self):
        transition = create_maximal_transition()
        transition.next_question.delete()
        self.assertTrue(Transition.objects.exists())

    def test_transition_is_not_removed_when_solution_is_removed(self):
        transition = create_maximal_transition()
        transition.solution.delete()
        self.assertTrue(Transition.objects.exists())


class QuestionnaireIndexViewTests(TestCase):
    def test_no_questionnaires(self):
        response = self.client.get(reverse("questionnaire:index"))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["questionnaire_list"], [])

    def test_one_questionnaire(self):
        name = "Test questionnaire"
        create_questionnaire(name, create_object(Question, "question_content"))
        response = self.client.get(reverse("questionnaire:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, name)
        self.assertEqual(len(response.context["questionnaire_list"]), 1)

    def test_two_questionnaire(self):
        name1 = "Test questionnaire 1"
        create_questionnaire(name1, create_object(Question, "question_content"))
        name2 = "Test questionnaire 2"
        create_questionnaire(name2, create_object(Question, "question_content"))

        response = self.client.get(reverse("questionnaire:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, name1)
        self.assertContains(response, name2)
        self.assertEqual(len(response.context["questionnaire_list"]), 2)


class QuestionnaireDetailViewTests(TestCase):
    def test_questionnaire(self):
        name = "Questionnaire name"
        question = create_object(Question, "question content")
        questionnaire = create_questionnaire(name, question)

        url = reverse("questionnaire:detail", args=(questionnaire.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, questionnaire.name)
        self.assertContains(response, questionnaire.first_question.content)


class QuestionViewTests(TestCase):
    def test_question(self):
        question = create_object(Question, "question content")
        for i in range(10):
            create_transition(question, create_object(Answer, "answer {}".format(i)))
        url = reverse("questionnaire:question_detail", args=(question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, question.content)
        for answer in map(lambda t: t.answer, question.transition_set.all()):
            self.assertContains(response, answer.content)


class SolutionViewTests(TestCase):
    def test_solution(self):
        solution = create_object(Solution, "solution content")
        url = reverse("questionnaire:solution_detail", args=(solution.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, solution.content)


class SolutionAndNextQuestionViewTests(TestCase):
    def test_maximal_transition(self):
        transition = create_maximal_transition()
        create_transition(transition.next_question, create_object(Answer, "next question answer content"))
        url = reverse("questionnaire:solution_and_next_question", args=(transition.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, transition.next_question.content)
        answers = [*map(lambda t: t.answer, transition.next_question.transition_set.all())]
        for answer in answers:
            self.assertContains(response, answer.content)
        self.assertContains(response, transition.solution)

