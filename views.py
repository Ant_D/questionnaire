from django.views import generic

from .models import Question, Questionnaire, Solution, Transition


class IndexView(generic.ListView):
    model = Questionnaire
    template_name = "questionnaire/index.html"


class DetailView(generic.DetailView):
    model = Questionnaire
    template_name = "questionnaire/detail.html"


class QuestionView(generic.DetailView):
    model = Question
    template_name = "questionnaire/question.html"


class SolutionView(generic.DetailView):
    model = Solution
    template_name = "questionnaire/solution.html"


class SolutionAndNextQuestionView(generic.DetailView):
    model = Transition
    template_name = "questionnaire/solution_and_next_question.html"
